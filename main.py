from src.ml.Globocom import Globocom
from src.ml.Lastfm import Lastfm
from src.ml.uids import uids
from src.util.FileLoad import FileLoad
from src.util.FileReader import FileReader
from src.util.Helpers import Helpers

def main():

    fileLoad = FileLoad()
    fileReader = FileReader()
    helpers = Helpers()

    print("Initializing method to Learning Profiles behind Shared Accounts")

    #uids.setMinSize(20)
    #print(uids.getMinSize())

    ##Globo
    globo = Globocom()
    parser_globo = globo.read_files()

    #np_uids, file = uids.generate('user_id', parser_globo)
    #uids.saveFile(np_uids, file)

    npuids_file_globo = fileLoad.loadFile("np_uids_20_globocom.pickle")
    df_reduced = globo.get_df_reduced(parser_globo, npuids_file_globo)

    globo.build_simulated_accounts(npuids_file_globo)

    articles_meta_df = fileReader.read("./resources/globocom/articles_metadata.csv")
    sample_sessions2 = fileLoad.loadFile("df_simulated_sessions_2Users.pickle")
    sample_sessions3 = fileLoad.loadFile("df_simulated_sessions_3Users.pickle")
    sample_sessions4 = fileLoad.loadFile("df_simulated_sessions_4Users.pickle")

    #globo.generate_accountIds(articles_meta_df, sample_sessions2, "df_sessions_accountId_2Users.pickle")
    #globo.generate_accountIds(articles_meta_df, sample_sessions3, "df_sessions_accountId_3Users.pickle")
    #globo.generate_accountIds(articles_meta_df, sample_sessions4, "df_sessions_accountId_4Users.pickle")

    ##lastfm
    lastfm = Lastfm()
    parser_lastfm = lastfm.reader_file()
    parser_lastfm = parser_lastfm.head(100000)

    #np_uids, file = uids.generate('user_id', parser_lastfm)
    #uids.saveFile(np_uids, file)

    npuids_file_lastfm = fileLoad.loadFile("np_uids_20_lastfm.pickle")
    user_sessions_df = lastfm.create_sessions_by_time(parser_lastfm)
    lastfm.build_simulated_accoounts(user_sessions_df, npuids_file_lastfm)

    sample_sessions2_lastfm = fileLoad.loadFile("df_simulated_sessions_lastfm_2Users.pickle")
    sample_sessions3_lastfm = fileLoad.loadFile("df_simulated_sessions_lastfm_3Users.pickle")
    sample_sessions4_lastfm = fileLoad.loadFile("df_simulated_sessions_lastfm_4Users.pickle")

    lastfm.generate_accountIds(sample_sessions2_lastfm, "df_sessions_accountId_2Users_lastfm.pickle")
    lastfm.generate_accountIds(sample_sessions3_lastfm, "df_sessions_accountId_3Users_lastfm.pickle")
    lastfm.generate_accountIds(sample_sessions4_lastfm, "df_sessions_accountId_4Users_lastfm.pickle")

    print("Starting session break")

    ###########GLOBO

    # Read features
    tsne_result = fileReader.read("cutSessions/resources_globo_simulated_sessions_data_tsne_results.pickle")

    # get cluster centers and label it with unique names
    cluster_centers, labels, tsne_clicks = globo.get_clusters_centers(tsne_result, parser_globo)
    df_center = globo.get_random_labels(cluster_centers)

    df_labeled_articles = globo.get_labeled_articles(tsne_clicks)
    globo.get_corresponding_centroid_labels(df_labeled_articles, df_center)
    helpers.rename_column('article_id', 'click_article_id', df_labeled_articles)

    # reduces size of dataframe with only sampled users
    globo.replace_index(df_reduced, df_labeled_articles)
    globo.map_euclidean(sample_sessions2)
    globo.map_euclidean(sample_sessions3)
    globo.map_euclidean(sample_sessions4)

    globo.get_cut_sessions(sample_sessions2)
    globo.get_cut_sessions(sample_sessions3)
    globo.get_cut_sessions(sample_sessions4)

    ###########LASTFM

    #generating tsne
    tsne_result_lastfm = lastfm.generate_tsne(user_sessions_df, parser_lastfm)
    #tsne_result_lastfm = fileLoad.loadFile("tsne_results_lastfm_artist_track_gp_merged.pickle")
    df_labeled_articles, df_center, cluster_centers, labels, tsne_clicks_sp = lastfm.get_clusters_centers(parser_lastfm, tsne_result_lastfm)
    lastfm.get_corresponding_centroid_labels(df_labeled_articles, df_center)


    print("Starting clustering step")

if __name__ == '__main__':
    main()