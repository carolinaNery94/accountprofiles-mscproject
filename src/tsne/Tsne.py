import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import NMF
from src.util.FileSave import FileSave
from sklearn.manifold import TSNE

class Tsne:

    def __init__(self):
        self._fileSave = FileSave()
        self._ntopics=6
        self._seed = 42
        self._ngram_range = (1,3)
        self._ncols =2

    def manipulate(self, tsne_results, np_article_ids):
        tsne_clicks = np.ndarray((np_article_ids.shape[0], 2), dtype=float)
        for i, item in enumerate(np_article_ids):
            tsne_clicks[i] = tsne_results[item]
        return tsne_clicks

    def get_np_article_ids(self, df_clicks, id):
        df_clicks.reset_index(inplace=True, drop=True)
        np_article_ids = np.array(df_clicks[id])
        np_article_ids.sort()
        np_article_ids = np.unique(np_article_ids)

        return np_article_ids

    def prepare_data(self, user_sessions_df, param1, param2):
        artist_track_df = user_sessions_df[[param1, param2]]
        artist_track_df_dd = artist_track_df.drop_duplicates([param1, param2])
        artist_track = artist_track_df_dd.groupby(param1)[param2]
        artist_track_gp = artist_track_df_dd.groupby(param1)[param2].agg(lambda x: ','.join(x))
        artist_track.apply(pd.DataFrame)

        return artist_track, artist_track_gp

    def calc_tfidf_nmf_return_xReduced(self, data, file_name):
        tfidf = TfidfVectorizer(max_df=0.6)
        X = tfidf.fit_transform(data)
        nmf = NMF(n_components=self._ntopics, random_state=self._seed, alpha=.1, l1_ratio=.5, init='nndsvd')
        x_reduced = nmf.fit_transform(X)

        self._fileSave.saveFile(file_name, x_reduced)

        return x_reduced

    def fit_transform_embbeding(self, x_reduced, file_name):
        tsne = TSNE(n_components=2, init='random', perplexity=50, n_iter=1000, random_state=self._seed)
        trans_data = tsne.fit_transform(x_reduced)

        self._fileSave.saveFile(file_name, trans_data)

        print('result of tsne: ', trans_data)

        return trans_data







