from src.cutSessions.Euclidean import Euclidean
import pandas as pd

class CutSessions:

    def __init__(self):
        self._euclidean = Euclidean()

    def map_euclidean(self, sample_sessions):
        list(map(self._euclidean.euclidean_from_centroid, sample_sessions))

    def get_cut_session_and_generate_newId_(self, sessions: list, cutoff: float):
        cut_sessions = []
        sessions_after_ = []
        sessions_after_f = []
        for session in sessions:
            np_sessions = self.get_sessions_in_sample_sessions(session)
            for s1 in range(0, len(np_sessions)):
                print('s1: ', s1)
                df = session[session['session_id'] == np_sessions[s1]]
                df = df.reset_index()
                print('df: ', df)

                store_index = df['index']
                print('store_index: ', store_index)

                cuts = df[df['distance'] > cutoff]
                cutoff_index = cuts.index[0] if len(cuts) > 0 else None

                if cutoff_index != None:
                    realCutoff = cutoff_index
                    df_test_previous = session[session['session_id'] == session['session_id'].values[cutoff_index]]
                    df_test = df[df['session_id'] == df['session_id'].values[cutoff_index]]
                    df_len = len(df_test)

                    x = '_000'
                    newId = str(df['session_id'].values[cutoff_index]) + x
                    print('newId: ', newId)

                    print('df_test: ', df_test)
                    print('realCutoff: ', realCutoff)
                    print('df_len: ', df_len)

                    print(df_test.loc[str(realCutoff):str(df_len), 'session_id'])

                    df_test.loc[realCutoff:df_len, 'session_id'] = newId
                    print('df_test after: ', df_test)
                    begin = df_test_previous[:1]
                    print('begin: ', begin)
                    until = df_test_previous[-1:]
                    print('until: ', until)

                    store_index = df_test

                    store_index = store_index.set_index('index')
                print('store_index: ', store_index)
                sessions_after_.append(store_index)
            list_result = pd.concat(sessions_after_, axis=0)
            sessions_after_f.append(list_result)
        return sessions_after_f

    def get_sessions_in_sample_sessions(self, session):
        session.dropna()
        session.reset_index(inplace=True, drop=True)
        return session.session_id.unique()