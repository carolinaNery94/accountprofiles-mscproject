import numpy as np

class Euclidean:

    def __init__(self):
        pass

    def euclidean_from_centroid(self, session):
        delta_x = session['x_centroid'] - session['x_centroid'].shift(1)
        delta_y = session['y_centroid'] - session['y_centroid'].shift(1)
        session['distance'] = self.euclidean(delta_x, delta_y)
        session['distance'].fillna(value=0, inplace=True)
        return session

    def euclidean(self, x, y):
        return np.sqrt(pow(x, 2) + pow(y, 2))