import pandas as pd
import pickle


class Helpers:

    def __init__(self):
        pass

    @staticmethod
    def build_simulated_sessions(uids, df_clicks, path, step=1, increment=None):
        print('[ INFO ] building simulated sessions')
        sample_sessions = []
        for index in range(0, len(uids) - step, increment):
            df1 = df_clicks[df_clicks['user_id'] == uids[index]]
            df2 = df_clicks[df_clicks['user_id'] == uids[index + 1]]
            df_concatenated = pd.concat([df1, df2])
            df_concatenated.reset_index(inplace=True, drop=True)
            sample_sessions.append(df_concatenated)
            if (index % 1000) == 0:
                print(index)
        print(sample_sessions)
        with open(path, "wb") as f:
            pickle.dump(sample_sessions, f)
        return sample_sessions

    @staticmethod
    def build_simulated_sessions_three_users(uids, df_clicks, path, step=1, increment=None):
        print('[ INFO ] building simulated sessions')
        sample_sessions = []
        for index in range(0, len(uids) - step, increment):
            df1 = df_clicks[df_clicks['user_id'] == uids[index]]
            df2 = df_clicks[df_clicks['user_id'] == uids[index + 1]]
            df3 = df_clicks[df_clicks['user_id'] == uids[index + 2]]
            df_concatenated = pd.concat([df1, df2, df3])
            df_concatenated.reset_index(inplace=True, drop=True)
            sample_sessions.append(df_concatenated)
            if (index % 1000) == 0:
                print(index)
        print(sample_sessions)
        with open(path, "wb") as f:
            pickle.dump(sample_sessions, f)
        return sample_sessions

    @staticmethod
    def build_simulated_sessions_four_users(uids, df_clicks, path, step=3, increment=None):
        print('[ INFO ] building simulated sessions')
        sample_sessions = []
        for index in range(0, len(uids) - step, increment):
            df1 = df_clicks[df_clicks['user_id'] == uids[index]]
            df2 = df_clicks[df_clicks['user_id'] == uids[index + 1]]
            df3 = df_clicks[df_clicks['user_id'] == uids[index + 2]]
            df4 = df_clicks[df_clicks['user_id'] == uids[index + 3]]
            df_concatenated = pd.concat([df1, df2, df3, df4])
            df_concatenated.reset_index(inplace=True, drop=True)
            sample_sessions.append(df_concatenated)
            if (index % 1000) == 0:
                print(index)
        print(sample_sessions)
        with open(path, "wb") as f:
            pickle.dump(sample_sessions, f)
        return sample_sessions

    def rename_column(self, colum1: str, colum2: str, fileToRename):
        fileToRename.rename(columns={colum1:colum2}, inplace=True)

