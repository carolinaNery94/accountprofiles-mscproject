import pickle

class FileLoad:

    def __init__(self):
        pass

    @staticmethod
    def loadFile(path: str):
        return pickle.load(open(path, "rb"))