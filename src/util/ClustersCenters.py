import pandas as pd
from sklearn.cluster import AffinityPropagation
import numpy as np
import random
import itertools
from src.util.Labels import Labels

class ClustersCenters:

    def __init__(self):
        self._labels = Labels()

    def get_cluster_centers(self, tsne_clicks, damping=0.5, sample=2000):
        print("[ INFO ] Finding cluster centers trough affinity propagation")
        # clustering
        tsne_clicks_sample = self.get_sample(tsne_clicks, sample)
        clustering = AffinityPropagation(damping).fit(tsne_clicks_sample)
        cluster_centers_indices = clustering.cluster_centers_indices_
        labels = clustering.labels_
        n_clusters_ = len(cluster_centers_indices)
        print("labels: ", labels.size)
        print("n_clusters: ", n_clusters_)
        # df['index'] = pd.DataFrame(cluster_centers_indices[:])
        # df[['X','Y']] = pd.DataFrame(tsne_clicks_sample[cluster_centers_indices[:]])
        # return df
        return tsne_clicks_sample[cluster_centers_indices[:]], labels

    @staticmethod
    def get_sample(np_array, size):
        df_array = pd.DataFrame(np_array)
        df_array_sample = df_array.sample(size)
        np_array_sample = np.ndarray((df_array_sample.shape[0], 2), dtype=float)
        np_array_sample = df_array_sample.sort_index().values
        return np_array_sample

    def get_random_labels(self, amount=10, deterministic=True):
        colors = ["red", "green", "blue", "cyan", "magenta", "yellow", "black", "white", "orange", "pink", "purple",
                  "aqua", "amber", "lazuli", "vanilla", "gray", "tan", "tangerine", "sand", "golden", "ginger", "rose",
                  "ruby", "fuchsia", "salmon", "violet", "sky", "indigo", "lapis", "teal", "mint", "brown", "graphite"]
        topics = ["topics", "stories", "news", "contents", "subjects", "themes", "discussions", "novels", "articles",
                  "phenomenons", "gossips", "rumors", "events", "occurrences", "incidents", "episodes", "disasters",
                  "plots", "accidents"]
        prod = ["{}-{}".format(item[0], item[1]) for item in itertools.product(colors, topics)]
        if deterministic:
            random.seed(1)
        random.shuffle(prod)
        return prod[:amount]

    def set_random_labels(self, cluster_centers):
        df_center = pd.DataFrame(cluster_centers, columns=['X', 'Y'])
        df_center['label'] = self.get_random_labels(df_center.shape[0])
        while df_center['label'].unique().shape[0] < df_center.shape[0]:
            df_center['label'] = self.get_random_labels(df_center.shape[0])

        return df_center

    # get corresponding centroid labels
    def get_corresponding_centroid_labels(self, df_labeled_articles, df_center):
        print("[ INFO ] Applying ficticious topic labels")
        df_labeled_articles['label'] = ''
        df_labeled_articles['label'] = df_labeled_articles.apply(lambda row: self._labels.get_nearest_label(row, df_center), axis=1)
