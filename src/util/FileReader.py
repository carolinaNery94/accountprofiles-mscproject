from src.util.Environment import Environment
import pandas as pd

class FileReader:
    __FILE_PATH = "./resources/" + Environment.get_environment() + "/"

    def __init__(self):
        pass

    def read(self, file: str):
        return pd.read_csv(file)

    def read_withParameters(self, file, sep, names, error_bad_lines):
        pd.read_csv(file, sep=sep, names=names, error_bad_lines=error_bad_lines)

    def concatFile(self, file: str, file2: str):
        return pd.concat([file, file2])

    def mergeFile(self, df, pdName, left_on: str, right_on: str):
        return df.merge(pdName, left_on=left_on, right_on=right_on)

