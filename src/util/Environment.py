class Environment:
    GLOBOCOM = 'globocom'
    LASTFM = 'lastfm'
    TAG_LASTFM = 'tagLastfm'

    @staticmethod
    def get_environment():
        return Environment.LASTFM