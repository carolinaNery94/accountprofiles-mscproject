import pickle
class FileSave:

    def __init__(self):
        pass

    @staticmethod
    def saveFile(file_name, data):
        with open(file_name, "wb") as f:
            pickle.dump(data, f)