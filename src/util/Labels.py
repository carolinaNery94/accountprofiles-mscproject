import numpy as np
import string
import random
import itertools

class Labels:

    def __init__(self):
        pass

    def get_nearest_label(self, articles_row, df_center: pd.DataFrame):
        delta_x = articles_row['X'] - df_center['X']
        delta_y = articles_row['Y'] - df_center['Y']
        euclidean = lambda x, y: np.sqrt(pow(x, 2) + pow(y, 2))
        distances = euclidean(delta_x, delta_y)
        index = distances[distances == distances.min()].index[0]
        return df_center['label'][index]

    def get_corresponding_centroid_coordinates(self, df_labeled_articles, df_center):
        df_labeled_articles['x_centroid'] = df_labeled_articles['label'].map(df_center.set_index('label')['X'])
        df_labeled_articles['y_centroid'] = df_labeled_articles['label'].map(df_center.set_index('label')['Y'])

        print(df_labeled_articles)

    def replace_index(self, df_reduced, df_labeled_articles):
        df_reduced['x_centroid'] = df_reduced['click_article_id'].map(
            df_labeled_articles.set_index('click_article_id')['x_centroid'])
        df_reduced['y_centroid'] = df_reduced['click_article_id'].map(
            df_labeled_articles.set_index('click_article_id')['y_centroid'])

        df_reduced.sort_values(by='click_timestamp', inplace=True)
        df_reduced.reset_index(inplace=True, drop=True)

    def id_generator(self, topics_v, size=6, chars=string.ascii_uppercase + string.digits):
        str_ = ''.join(random.choice(chars) for _ in range(size))
        topics_v.append(str_)
        return str_

    def get_random_labels(self, topics_v, amount=10, deterministic=True):
        colors = ["red", "green", "blue", "cyan", "magenta", "yellow", "black", "white", "orange", "pink", "purple",
                  "aqua", "amber", "lazuli", "vanilla", "gray", "tan", "tangerine", "sand", "golden", "ginger", "rose",
                  "ruby", "fuchsia", "salmon", "violet", "sky", "indigo", "lapis", "teal", "mint", "brown", "graphite"]
        prod = ["{}-{}".format(item[0], item[1]) for item in itertools.product(colors, topics_v)]
        if deterministic:
            random.seed(1)
        random.shuffle(prod)
        return prod[:amount]

