from src.util.Helpers import Helpers

class GenerateAccountsMock:

    def __init__(self):
        self._helper = Helpers()

    def generate_simulatedSessionAccount_2users(self, np_uids, df_reduced, path, step):
        self._helper.build_simulated_sessions(np_uids, df_reduced, path, step, 2)

    def generate_simulatedSessionAccount_3users(self, np_uids, df_reduced, path, step):
        self._helper.build_simulated_sessions_three_users(np_uids, df_reduced, path, step, 3)

    def generate_simulatedSessionAccount_4users(self, np_uids, df_reduced, path, step):
        self._helper.build_simulated_sessions_three_users(np_uids, df_reduced, path, step, 4)

