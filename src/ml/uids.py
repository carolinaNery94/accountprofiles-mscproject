import numpy as np
import pickle
from src.util.Environment import Environment


class uids:

    __MIN_SIZE = None

    def __init__(self, minSizeParam=20):
        self.minSizeParam = minSizeParam

    @staticmethod
    def setMinSize(minSize):
        uids.__MIN_SIZE = minSize

    @staticmethod
    def getMinSize():
        if uids.__MIN_SIZE is None:
            uids.__MIN_SIZE = uids()
        return uids.__MIN_SIZE

    @staticmethod
    def filter_sessions(file: str, uniqueFilter: str):
        file.reset_index(inplace=True, drop=True)
        np_unique_uids = file[uniqueFilter].unique()
        np.random.seed(1)
        np_unique_uids = np.random.choice(np_unique_uids, len(np_unique_uids), replace=False)
        np_uids = np.array([])
        p = 1
        total_length = p*len(np_unique_uids)

        return file, np_unique_uids, total_length, np_uids

    @staticmethod
    def generate(uniqueFilter: str, file: str):
        file, np_unique_uids, total_length, np_uids = uids.filter_sessions(file, uniqueFilter)

        count = 0
        for uid in np_unique_uids:
            if len(file[file[uniqueFilter] == uid]) > uids.__MIN_SIZE:
                np_uids = np.append(np_uids, uid)
            if len(np_uids) == total_length:
                break
            if count % 1000 == 0:
                print(count)
            count += 1
        return np_uids, file

    @staticmethod
    def saveFile(np_uids, file):
        with open("np_uids_{}_{}.pickle".format(uids.__MIN_SIZE, Environment.get_environment()), "wb") as file:
            pickle.dump(np_uids, file)



