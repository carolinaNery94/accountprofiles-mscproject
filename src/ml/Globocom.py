from src.util.Environment import Environment
from src.util.FileReader import FileReader
import pandas as pd
import glob
from src.ml.GenerateAccountsMock import GenerateAccountsMock
from src.ml.account.GenerateAccountIds import GenerateAccountIds
from src.util.ClustersCenters import ClustersCenters
from src.tsne.Tsne import Tsne
from src.util.Labels import Labels
from src.cutSessions.CutSessions import CutSessions

class Globocom:
    __FILES = "./resources/" + Environment.get_environment() + "/clicks/*.csv"

    __CUTOFF = 40

    def __init__(self):
        self._reader = FileReader()
        self._accountsMock = GenerateAccountsMock()
        self._generateAccountIds = GenerateAccountIds()
        self._clusters_centers = ClustersCenters()
        self._cutSessions = CutSessions()
        self._labels = Labels()
        self._tsne = Tsne()
        self._account_col = 'account_id'
        self._user_col = 'user_id'
        self._session_col = 'session_id'
        self._item_col = 'category_id'
        self._click_article_id = 'click_article_id'
        self._article_id = 'article_id'
        self._damping = 0.9
        self._np_article_ids = ''
        self._df_reduced = ''

    def read_files(self):
        files = glob.glob(self.__FILES)

        print("[INFO] reading dataset")
        dfclicks = pd.DataFrame()

        for file in files:
            lines = self._reader.read(file)
            dfclicks = self._reader.concatFile(dfclicks, lines)
            print(dfclicks)
            print("finish reading")
        return dfclicks

    def get_df_reduced(self, df_clicks, np_uids):
        df_reduced = df_clicks[df_clicks['user_id'].isin(np_uids)]

        df_reduced.sort_values(by='click_timestamp', inplace=True)
        df_reduced.reset_index(inplace=True, drop=True)
        self._df_reduced = df_reduced

        return self._df_reduced

    def build_simulated_accounts(self, np_uids):
        sample_sessions2 = self._accountsMock.generate_simulatedSessionAccount_2users(np_uids, self._df_reduced, "df_simulated_sessions_2Users.pickle", 2)
        sample_sessions3 = self._accountsMock.generate_simulatedSessionAccount_3users(np_uids, self._df_reduced, "df_simulated_sessions_3Users.pickle", 3)
        sample_sessions4 = self._accountsMock.generate_simulatedSessionAccount_3users(np_uids, self._df_reduced, "df_simulated_sessions_4Users.pickle", 4)

        return sample_sessions2, sample_sessions3, sample_sessions4

    # should be called after generate sessions to simulate shared accounts
    def generate_accountIds(self, article_meta_df, sample_sessions, file_name):
        clicks_df_2 = ''
        for index, session in enumerate(sample_sessions):
            clicks_df_2 = session[0].merge(article_meta_df, left_on='click_article_id', right_on='article_id')
        self._generateAccountIds.generate_accountIds(sample_sessions, self._item_col, self._session_col, self._user_col, self._account_col, file_name, clicks_df_2)

    # get cluster centers and label it with unique names
    def get_clusters_centers(self, tsne_results, df_clicks):
        self._np_article_ids = self._tsne.get_np_article_ids(df_clicks,  self._click_article_id)
        tsne_clicks = self._tsne.manipulate(tsne_results, self._np_article_ids)

        cluster_centers, labels = self._clusters_centers.get_cluster_centers(tsne_clicks, self._damping, sample=10000)

        print('cluster_centers: ')
        print(cluster_centers)
        return cluster_centers, labels, tsne_clicks

    def get_random_labels(self, cluster_centers):
        df_center = self._clusters_centers.set_random_labels(cluster_centers)
        return df_center

    def get_labeled_articles(self, tsne_clicks):
        df_labeled_articles = pd.DataFrame(self._np_article_ids, columns=['article_id'])
        df_labeled_articles[['X', 'Y']] = pd.DataFrame(tsne_clicks)
        print('df_labeled_articles: ', df_labeled_articles)

        return df_labeled_articles

    def get_corresponding_centroid_labels(self, df_labeled_articles, df_center):
        self._clusters_centers.get_corresponding_centroid_labels(df_labeled_articles, df_center)

    def replace_index(self, df_reduced, df_labeled_articles):
        self._labels.replace_index(df_reduced, df_labeled_articles)

    def map_euclidean(self, sample_sessions):
        self._cutSessions.map_euclidean(sample_sessions)

    def get_cut_sessions(self, sample_sessions):
        self._cutSessions.get_cut_session_and_generate_newId_(sample_sessions, self.__CUTOFF)

