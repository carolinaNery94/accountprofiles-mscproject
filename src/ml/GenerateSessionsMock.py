class GenerateSessionsMock:

    def __init__(self):
        pass

    @staticmethod
    def create_sessions_by_time(df, id_col, timestamp_col, timedelta, session_col):
        ordered_df = df.sort_values([id_col, timestamp_col])
        ordered_df['deltatime'] = ordered_df.groupby([id_col])[timestamp_col].diff()
        ordered_df[session_col] = (ordered_df['deltatime'] > timedelta)
        ordered_df[session_col] = ordered_df[session_col].cumsum()
        return ordered_df.drop(columns=['deltatime'])



