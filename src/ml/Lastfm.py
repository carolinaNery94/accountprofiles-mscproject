from src.util.Environment import Environment
from src.util.FileReader import FileReader
import pandas as pd
from src.ml.GenerateAccountsMock import GenerateAccountsMock
from src.ml.GenerateSessionsMock import GenerateSessionsMock
from src.ml.account.GenerateAccountIds import GenerateAccountIds
from src.tsne.Tsne import Tsne
import numpy as np
from src.util.FileSave import FileSave
from src.util.ClustersCenters import ClustersCenters
from src.util.Labels import Labels

class Lastfm:
    __FILE = "./resources/" + Environment.get_environment() + "/userid-timestamp-artid-artname-traid-traname.tsv"

    def __init__(self):
        self._reader = FileReader()
        self._accountsMock = GenerateAccountsMock()
        self._sessionsMock = GenerateSessionsMock()
        self._generateAccountIds = GenerateAccountIds()
        self._clusters_centers = ClustersCenters()
        self._labels = Labels()
        self._fileSave = FileSave()
        self._tsne = Tsne()
        self._account_col = 'account_id'
        self._user_col = 'user_id'
        self._session_col = 'session_id'
        self._item_col = 'track_name'
        self._timestamp = 'timestamp'
        self._artist_name = 'artist_name'
        self._track_name = 'track_name'
        self._damping = 0.9

    def reader_file(self):
        dfUser_listenings = pd.read_csv('./resources/lastfm/userid-timestamp-artid-artname-traid-traname.tsv', sep='\t',
                       names=['user_id', 'timestamp', 'artist_id', 'artist_name', 'track_id', 'track_name'],
                       error_bad_lines=False)

        print(dfUser_listenings['timestamp'])
        return dfUser_listenings

    def create_sessions_by_time(self, user_listenings):
        user_listenings[self._timestamp] = pd.to_datetime(user_listenings[self._timestamp])
        user_sessions_df = self._sessionsMock.create_sessions_by_time(user_listenings, self._user_col, self._timestamp, pd.Timedelta(minutes=30), self._session_col)

        user_sessions_df.reset_index(inplace=True, drop=True)
        return user_sessions_df

    def build_simulated_accoounts(self, user_sessions_df, np_uids):
        df_reduced = user_sessions_df[user_sessions_df[self._user_col].isin(np_uids)]

        df_reduced.sort_values(by='timestamp', inplace=True)
        df_reduced.reset_index(inplace=True, drop=True)

        sample_session2 = self._accountsMock.generate_simulatedSessionAccount_2users(np_uids, df_reduced, "df_simulated_sessions_lastfm_2Users.pickle", 2)
        sample_sessions3 = self._accountsMock.generate_simulatedSessionAccount_3users(np_uids, df_reduced, "df_simulated_sessions_lastfm_3Users.pickle", 3)
        sample_sessions4 = self._accountsMock.generate_simulatedSessionAccount_4users(np_uids, df_reduced, "df_simulated_sessions_lastfm_4Users.pickle", 4)

        return sample_session2, sample_sessions3, sample_sessions4

    def generate_accountIds(self, sample_sessions, file_name):
        self._generateAccountIds.generate_accountIds(sample_sessions, self._item_col, self._session_col, self._user_col, self._account_col, file_name)

    def generate_tsne(self, user_sessions_df, user_listenings):
        artist_track, artist_track_gp = self._tsne.prepare_data(user_sessions_df, self._artist_name, self._track_name)

        artist_track_gp = pd.DataFrame(artist_track_gp)
        artist_track_gp_merge = artist_track_gp.merge(user_listenings, left_on='artist_name', right_on='artist_name')

        data = artist_track_gp_merge.track_name_x
        print('len(data):', len(data))
        print(artist_track_gp_merge.shape)
        print(data.shape)

        x_reduced = self._tsne.calc_tfidf_nmf_return_xReduced(data, "lastfm_embeddings.pickle")
        embeddings = x_reduced

        tsne_result = self._tsne.fit_transform_embbeding(embeddings, "tsne_results_lastfm_artist_track_gp_merged.pickle")

        return tsne_result

    def get_clusters_centers(self, user_listenings, tsne_results):
        np_track_id_name_sp = np.array(user_listenings[self._track_name])
        d = {ni: indi for indi, ni in enumerate(set(np_track_id_name_sp))}
        numbers_sp = [d[ni] for ni in np_track_id_name_sp]

        print('len(numbers_sp):', len(numbers_sp))
        np_track_id_sp = numbers_sp

        tsne_clicks_sp = np.ndarray((np_track_id_name_sp.shape[0], 2), dtype=float)

        for i, item in enumerate(np_track_id_sp):
            if i >= len(tsne_results):
                break
            else:
                tsne_clicks_sp[i] = tsne_results[item]

        print('escrevendo tsne_clicks_sp')
        self._fileSave.saveFile("tsne_clicks_sp.pickle", tsne_clicks_sp)

        # get cluster centers and label it with unique names
        cluster_centers, labels = self._clusters_centers.get_cluster_centers(tsne_clicks_sp, self._damping, sample=10000)
        cluster_centers_ = np.ndarray((cluster_centers.shape[0], 2), dtype=float)
        print('cluster_centers_.shape', cluster_centers_.shape)

        self._fileSave.saveFile("cluster_centers.pickle", cluster_centers_)

        num = cluster_centers.shape[0] / 2
        print(int(num))

        topics_v = []
        x = num
        i = 0
        while i < x:
            self._labels.id_generator(topics_v, size=6)
            i += 1
        print(len(topics_v))

        df_center = pd.DataFrame(cluster_centers, columns=['X', 'Y'])
        df_center['label'] = self._labels.get_random_labels(df_center.shape[0])

        while df_center['label'].unique().shape[0] < df_center.shape[0]:
            df_center['label'] = self._labels.get_random_labels(df_center.shape[0])

        print('df_center:', df_center)
        self._fileSave.saveFile("df_center", df_center)

        df_labeled_articles = pd.DataFrame(np_track_id_name_sp, columns=['track_name'])
        print(df_labeled_articles)

        df1 = df_labeled_articles.dropna()
        df_labeled_articles = df1
        self._fileSave.saveFile("df_labeled_articles", df_labeled_articles)

        df_labeled_articles[['X', 'Y']] = pd.DataFrame(tsne_clicks_sp)
        print(df_labeled_articles)

        return df_labeled_articles, df_center, cluster_centers, labels, tsne_clicks_sp

    def get_corresponding_centroid_labels(self, df_labeled_articles, df_center):
        self._clusters_centers.get_corresponding_centroid_labels(df_labeled_articles, df_center)
























