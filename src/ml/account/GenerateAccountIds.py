import pandas as pd
from src.util.FileSave import FileSave

class GenerateAccountIds:

    def __init__(self):
        self._fileSave = FileSave()

    def generate_accountIds(self, sample_sessions, item_col, session_col, user_col, account_col, file_name, clicks_df_2=None):
        dfs = pd.DataFrame()
        account_ids = 0.0
        # articles_meta_df = pd.read_csv('../globocom/articles_metadata.csv') será representado por click_df_2 no caso da globo
        for index, session in enumerate(sample_sessions):
            df_items_aparitions = clicks_df_2.groupby(item_col).size().to_frame('item_aparitions').reset_index()
            df_items_aparitions = clicks_df_2.merge(df_items_aparitions)
            filtered_by_items = df_items_aparitions[df_items_aparitions.item_aparitions >= 1][
                [user_col, session_col, item_col]]
            df_items_in_session = filtered_by_items.groupby(session_col).size().to_frame('num_items').reset_index()
            df_items_in_session = filtered_by_items.merge(df_items_in_session)
            filtered_by_sessions = df_items_in_session[df_items_in_session.num_items >= 1][
                [user_col, session_col, item_col]]
            df_sessions_for_user = filtered_by_sessions[[user_col, session_col]].drop_duplicates() \
                .groupby(user_col).size().to_frame('num_sessions').reset_index()
            df_sessions_for_user = filtered_by_sessions.merge(df_sessions_for_user)
            filtered_by_users = df_sessions_for_user[df_sessions_for_user.num_sessions >= 1][
                [user_col, session_col, item_col]]
            filtered_clicks_df = filtered_by_users
            users = filtered_clicks_df[[user_col]].drop_duplicates().sample(frac=1, random_state=42)
            num_users = len(users)
            users_accounts_partition = users.iloc[0:num_users].copy()
            temp = users_accounts_partition.iloc[::2].copy()
            temp[account_col] = account_ids
            users_accounts_partition = users_accounts_partition.merge(temp, how='left').fillna(method='ffill')
            user_accounts = filtered_clicks_df[filtered_clicks_df[user_col].isin(users_accounts_partition[user_col])].merge(
                users_accounts_partition)

            dfs = dfs.append(user_accounts)
            account_ids += 1

            self._fileSave.saveFile(file_name, dfs)






